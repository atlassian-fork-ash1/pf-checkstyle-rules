# Product Fabric Checkstyle Rules

Checkstyle rules used by Product Fabric services.

Mainly build on top of Google coding conventions from [Google Java Style](https://google.github.io/styleguide/javaguide.html)